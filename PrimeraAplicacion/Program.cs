﻿using System;

namespace PrimeraAplicacion
{
    class Program
    {
        static void Main(string[] args)
        {
        
            Console.WriteLine("Introduce el primer numero");
            int num1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Introduce el segundo numero");
            int num2 = int.Parse(Console.ReadLine());

            Console.WriteLine("La suma es " + (num1 + num2));
        }
    }
}
